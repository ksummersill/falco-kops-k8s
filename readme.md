1. Setup the RBAC, Cluster, Service Accounts, and Bindings
```
kubectl apply -f falco-rbac.yml
```

2. Starting the Falco Service
```
kubectl apply -f falco-service.yaml 
```
Note: Located in the Service Directory

3. Create the ConfigMap with the Configuration Files
```
kubectl create configmap falco-config --from-file=deployments\falco\falco-configs -n monitoring
```

4. Last but least setup the Daemonset for Falco
```
kubectl apply -f falco-daemonset.yml
```
